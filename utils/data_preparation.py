import numpy as np
import torchvision
from PIL import Image
import os.path as path

class ImagePerturber:
    def __init__(self, base_path, min_corruption, max_corruption):
        """

        :param base_path: path to root folder of images.
        :param min_corruption: \in [0, 1] percentage of how many pixel may be corrupted at least
        :param max_corruption: \in [0, 1] percentage of how many pixel may be corrupted at most
        """
        self.min_corruption = min_corruption
        self.max_corruption = max_corruption

    def load_image(self, image_name):
        image = Image.open(image_name)
        return image

    def perturb_image(self, image, clusters, debug=False):
        pixel_map = image.load()
        all_pixels = image.size[0] * image.size[1]
        pixels_to_remove = int(all_pixels * np.random.uniform(self.min_corruption, self.max_corruption))
        pixels_per_cluster = pixels_to_remove / clusters
        upper_left_corners = [(int(np.random.uniform(0, image.size[0])),
                               int(np.random.uniform(0, image.size[1]))) for c in range(clusters)]
        for center in upper_left_corners:
            dim_0 = range(center[0], min(center[0] + int(np.sqrt(pixels_per_cluster)), image.size[0]))
            dim_1 = range(center[1], min(center[1] + int(np.sqrt(pixels_per_cluster)), image.size[0]))
            for i in dim_0:
                for j in dim_1:
                    pixel_map[i, j] = (0, 0, 0)
        if debug:
            image.show()
        return image


def create_samples(num_samples):
    cifar10 = torchvision.datasets.CIFAR10(root='./data', train=True, download=True)
    image_destroyer = ImagePerturber('', 0.2, 0.3)
    for i in range(num_samples):
        image = cifar10[i][0]
        clusters = np.random.choice([1, 2, 3])
        perturbed_image = image_destroyer.perturb_image(image, clusters, False)
        perturbed_image.save(path.join('image_samples', 'corrupt_{}'.format(i)), 'JPEG')

create_samples(4)