import os
import cv2
import numpy as np
from pathlib import Path

def downsample(src, dest, size=(64, 64)):
    Path(dest).mkdir(parents=True, exist_ok=True)
    for root, dirs, files in os.walk(src, topdown=False):
        for name in files:
            img = cv2.imread(os.path.join(root, name))
            res = cv2.resize(img, dsize=size, interpolation=cv2.INTER_NEAREST)
            cv2.imwrite(dest + '/' + name, res)


if __name__ == '-__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Configure the downsampler.')
    parser.add_argument('src', help='source folder')
    parser.add_argument('dst', type=str)
    parser.add_argument('size', type=int)
    args = parser.parse_args()
    downsample(args.src, args.dst, size=(args.size, args.size))

downsample(os.path.join(os.pardir, 'data', 'img_align_celeba'), os.path.join(os.pardir, 'data', 'celeba64'))
def split_train_test_dirs(root_dir):
    train_dir = os.path.join(root_dir, 'train')
    test_dir = os.path.join(root_dir, 'test')
    Path(train_dir).mkdir(exist_ok=True)
    Path(test_dir).mkdir(exist_ok=True)
    for file in os.listdir(root_dir):
        if file.endswith('.png'):
            folder = file.split('.png')[0].split('_')[1]
            os.rename(os.path.join(root_dir, file), os.path.join(root_dir, folder, file))

#split_train_test_dirs(os.path.join(os.pardir, 'data', 'qd_imd64'))

