from skimage.feature import canny
import numpy as np
from scipy.misc import imread
from skimage.color import rgb2gray, gray2rgb
from PIL import Image

img1 = imread("../samples/images/celeba_01.png")
mask2 = imread("../samples/masks/celeba_01.png")


def load_edges(img, index, mask, training=False):
    img = rgb2gray(img)
    mask2 = None if training else (1 - rgb2gray(mask) / 255).astype(np.bool)
    sigma = 1

    return canny(img, sigma=sigma, mask=mask2).astype(np.float), img, mask


edgeimage, _, _ = load_edges(img1, 0, mask2)
edgeimage = Image.fromarray(edgeimage * 255)
edgeimage.show()
