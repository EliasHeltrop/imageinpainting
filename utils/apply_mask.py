import os
import random
import cv2
import scipy.misc

base_mask_dir = "../qd_imd64/"
maskname = random.choice(os.listdir(base_mask_dir))

img = cv2.imread("../places64/Places365_val_00000001.jpg")
mask = cv2.imread(base_mask_dir + maskname, 0)

res = cv2.bitwise_and(img,img,mask = mask)
res = scipy.misc.toimage(res)
res.show()


