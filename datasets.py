from torch.utils.data import Dataset
import os
from skimage import io
from skimage.color import rgb2gray
import numpy as np
from skimage.feature import canny

"""
The following data_structure is assumed:
----------------------------------------
For the downsampled CelebA dataset:

data/celeba64/all-the-jpg-files
----------------------------------------
For the mask dataset:
data/qd_imd64/  -train/all-train.png
                -test/all-test.png
----------------------------------------
"""


class ImageDataset(Dataset):
    def __init__(self, root_dir):
        self.root_dir = root_dir

    def __len__(self):
        files = os.listdir(self.root_dir)
        return len(files)

    def __getitem__(self, idx):
        file_name = str(idx + 1).zfill(6)
        path = os.path.join(self.root_dir, file_name + '.jpg')
        image = io.imread(path)
        return image


class MaskDataset(Dataset):
    def __init__(self, root_dir):
        self.root_dir = root_dir

    def __len__(self):
        files = os.listdir(self.root_dir)
        return len(files)

    def __getitem__(self, idx):
        folder = self.root_dir.split(os.path.sep)[-1]
        idx_zeros = str(idx).zfill(5)
        file_name = idx_zeros + '_' + folder + '.png'
        path = os.path.join(self.root_dir, file_name)
        image = io.imread(path)
        image = rgb2gray(image)
        image = 1 - image
        return image


class TrainingDataset(Dataset):
    def __init__(self, img_root, mask_root):
        self.img_root = img_root
        self.masks = MaskDataset(mask_root)
        self.images = ImageDataset(img_root)
        self.n_masks = len(self.masks)

    def __len__(self):
        files = os.listdir(self.img_root)
        return len(files)

    def __getitem__(self, idx):
        random_index = np.random.choice(range(self.n_masks))
        mask = self.masks[random_index]

        image = self.images[idx] / 255
        gray_img = rgb2gray(image)
        edges = canny(gray_img, sigma=1).astype(np.float)
        #mask = np.reshape(mask, (mask.shape[0], mask.shape[1], 1))
        #np.reshape(gray_img, (image.shape[0], image.shape[1], 1))
        #edges = np.reshape(edge, (edge.shape[0], edge.shape[1], 1))
        return image, gray_img, edges, mask

