import torch
import torch.nn as nn


class ResnetBlock(nn.Module):
    def __init__(self, dim, spectral=False):
        super(ResnetBlock, self).__init__()
        #  Omitted spectral norm, used non-normalized  con2d
        self.block = nn.Sequential(
            nn.ReflectionPad2d(1),
            spectral_norm(nn.Conv2d(dim, dim, 3, bias=not spectral), spectral),
            nn.InstanceNorm2d(dim),
            nn.ReLU(),
            nn.ReflectionPad2d(1),
            spectral_norm(nn.Conv2d(dim, dim, 3, bias=not spectral), spectral),
            nn.InstanceNorm2d(dim)
        )
    def forward(self, x):
        x = x + self.block(x)
        return x


class Discriminator(nn.Module):
    def __init__(self, input_dim, spectral=False):
        super(Discriminator, self).__init__()
        #  Omitted spectral norm, used non-normalized  con2d
        stride = 4
        self.noise_std = 0.15
        self.conv1 = nn.Sequential(
            spectral_norm(nn.Conv2d(input_dim, out_channels=64, kernel_size=4, stride=stride, padding=1, bias=not spectral), spectral),
            nn.ReLU()
        )
        self.conv2 = nn.Sequential(
            spectral_norm(nn.Conv2d(64, out_channels=128, kernel_size=4, stride=stride, padding=1, bias=not spectral), spectral),
            nn.ReLU()
        )
        self.conv3 = nn.Sequential(
            spectral_norm(nn.Conv2d(128, out_channels=1, kernel_size=4, stride=stride, padding=1, bias=not spectral), spectral),
            nn.ReLU()
        )

    def forward(self, x):
        x = x + torch.randn(x.size()).to('cuda:0') * self.noise_std
        self.noise_std *= 0.9995
        conv1 = self.conv1(x)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        out = torch.sigmoid(conv3)
        return out, [conv1, conv2, conv3]


class EdgeGenerator(nn.Module):
    def __init__(self, n_blocks, spectral=False):
        super(EdgeGenerator, self).__init__()
        self.encoder = nn.Sequential(
            nn.ReflectionPad2d(3),
            spectral_norm(nn.Conv2d(in_channels=3, out_channels=16, kernel_size=7), spectral),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(in_channels=16, out_channels=64, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(64),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(128),
            nn.ReLU()
        )
        blocks = [ResnetBlock(128) for i in range(n_blocks)]
        self.blocks = nn.Sequential(*blocks)
        self.decoder = nn.Sequential(
            spectral_norm(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(64),
            nn.ReLU(),
            spectral_norm(nn.ConvTranspose2d(in_channels=64, out_channels=16, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
            nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=16, out_channels=1, kernel_size=7)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.blocks(x)
        x = self.decoder(x)
        return torch.sigmoid(x)

    
class InpaintGenerator(nn.Module):
    def __init__(self, n_blocks, spectral=False):
        super(InpaintGenerator, self).__init__()
        self.encoder = nn.Sequential(
            nn.ReflectionPad2d(3),
            spectral_norm(nn.Conv2d(in_channels=4, out_channels=16, kernel_size=7), spectral),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(in_channels=16, out_channels=64, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(64),
            nn.ReLU(),
            spectral_norm(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(128),
            nn.ReLU()
        )

        blocks = [ResnetBlock(128) for i in range(n_blocks)]
        self.blocks = nn.Sequential(*blocks)
        self.decoder = nn.Sequential(
            spectral_norm(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(64),
            nn.ReLU(),
            spectral_norm(nn.ConvTranspose2d(in_channels=64, out_channels=16, kernel_size=4, stride=2, padding=1), spectral),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
            nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=16, out_channels=3, kernel_size=7)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.blocks(x)
        x = self.decoder(x)
        return (torch.tanh(x) + 1) / 2

def spectral_norm(module, mode=True):
    if mode:
        return nn.utils.spectral_norm(module)
    return module
