from .networks import Discriminator, InpaintGenerator, EdgeGenerator
from .losses import AdversarialLoss, PerceptualLoss, StyleLoss
import torch
import torch.nn as nn
import torch.optim as optim

class EdgeModel(nn.Module):
    def __init__(self, n_blocks, lr, fm_weight):
        super(EdgeModel, self).__init__()
        n_channels = 2
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.generator = EdgeGenerator(n_blocks).to(self.device)
        self.discriminator = Discriminator(n_channels).to(self.device)
        self.l1_loss = nn.L1Loss()
        self.adv_loss = AdversarialLoss().to(self.device)
        self.gen_optim = optim.Adam(self.generator.parameters(), lr=lr, betas=(0.2, 0.999))
        self.dis_optim = optim.Adam(self.discriminator.parameters(), lr=lr, betas=(0.2, 0.999))
        self.fm_weight = fm_weight

    def epoch(self, images, edges, masks):
        self.gen_optim.zero_grad()
        self.dis_optim.zero_grad()

        generated_outputs = self(images, edges, masks)
        dis_loss, dis_fm = self.discriminator_loss(images, edges, generated_outputs)
        gen_loss, gen_fm = self.generator_loss(images, generated_outputs)
        fm_loss = 0.
        for i in range(len(dis_fm)):
            fm_loss += self.l1_loss(gen_fm[i], dis_fm[i].detach())
        fm_loss *= self.fm_weight
        gen_loss += fm_loss
        return generated_outputs, gen_loss, dis_loss

    def backward(self, gen_loss, dis_loss):
        dis_loss.backward()
        self.dis_optim.step()
        gen_loss.backward(retain_graph=True)
        self.gen_optim.step()


    def discriminator_loss(self, images, edges, generated):
        real_dis_inputs = torch.cat((images, edges), dim=1)
        gen_dis_inputs = torch.cat((images, generated.detach()), dim=1)

        real_dis_out, real_dis_fm = self.discriminator(real_dis_inputs)
        gen_dis_out, gen_dis_fm = self.discriminator(gen_dis_inputs)
        real_dis_loss = self.adv_loss(real_dis_out, True)
        gen_dis_loss = self.adv_loss(gen_dis_out, True)
        mean_loss = (real_dis_loss + gen_dis_loss) / 2
        return mean_loss, real_dis_fm

    def generator_loss(self, images, generated):
        gen_generator_input = torch.cat((images, generated), dim=1)
        output, generator_fm = self.discriminator(gen_generator_input)
        gan_loss = self.adv_loss(output, True)
        return gan_loss, generator_fm


    def forward(self, images, edges, masks):
        masked_edges = edges * (1 - masks)  # Assuming that masks are 1 where image should be zero
        masked_images = images * (1 - masks)
        concat = torch.cat((masked_images, masked_edges, masks), dim=1)
        outputs = self.generator(concat)
        return outputs

class InpaintingModel(nn.Module):
    def __init__(self, n_blocks, lr, adv_loss_weight, l1_loss_weight, style_loss_weight, perc_loss_weight):
        super(InpaintingModel, self).__init__()
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.generator = InpaintGenerator(n_blocks).to(self.device)
        self.discriminator = Discriminator(3).to(self.device)  # rgb images
        self.l1_loss = nn.L1Loss()
        self.l1_loss_weight = l1_loss_weight
        self.preceptual_loss = PerceptualLoss().to(self.device)
        self.perc_loss_weight = perc_loss_weight
        self.style_loss = StyleLoss().to(self.device)
        self.style_loss_weight = style_loss_weight
        self.adv_loss = AdversarialLoss()
        self.adv_loss_weight = adv_loss_weight

        self.gen_optim = optim.Adam(self.generator.parameters(), lr=lr, betas=(0.2, 0.999))
        self.dis_optim = optim.Adam(self.discriminator.parameters(), lr=lr, betas=(0.2, 0.999))

    def epoch(self, images, edges, masks):
        self.gen_optim.zero_grad()
        self.dis_optim.zero_grad()

        gen_images = self(images, edges, masks)
        dis_loss = self.discriminator_loss(images, gen_images)
        gen_loss = self.generator_loss(images, masks, gen_images)

        return gen_images, gen_loss, dis_loss

    def discriminator_loss(self, images, generated_images):
        real_dis_output, real_fm = self.discriminator(images)
        gen_dis_output, gen_fm = self.discriminator(generated_images.detach())
        real_dis_loss = self.adv_loss(real_dis_output, True)
        gen_dis_loss = self.adv_loss(gen_dis_output, False)
        dis_loss = (gen_dis_loss + real_dis_loss) / 2
        return dis_loss

    def generator_loss(self, images, masks, generated_images):
        gen_generator_output, gen_fm = self.discriminator(generated_images)
        gan_loss = self.adv_loss(gen_generator_output, True) * self.adv_loss_weight  # Why is that not False
        l1_loss = self.l1_loss(generated_images, images) * self.l1_loss_weight
        perc_loss = self.preceptual_loss(generated_images, images) * self.perc_loss_weight
        style_loss = self.style_loss(generated_images * masks, images * masks) * self.style_loss_weight  # Why?
        gen_loss = gan_loss + l1_loss + perc_loss + style_loss
        return gen_loss

    def forward(self, images, edges, masks):
        masked_images = images * (1 - masks).float() + masks
        inputs = torch.cat((masked_images, edges), dim=1)
        outputs = self.generator(inputs)
        return outputs

    def backward(self, gen_loss, dis_loss):
        dis_loss.backward()
        self.dis_optim.step()
        gen_loss.backward()
        self.gen_optim.step()



