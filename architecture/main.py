from skimage.color import rgb2gray
from skimage import io
from skimage.feature import canny
from architecture.models import EdgeModel, InpaintingModel
from datasets import TrainingDataset
from torch.utils.data import DataLoader, SubsetRandomSampler
import os
import torch
import numpy as np
import matplotlib.pyplot as plt
import cv2


class EdgeConnect:
    def __init__(self, n_blocks=5, batch_size=16, edge_lr=1e-4,
                 inpaint_lr=1e-4, fm_loss_w=10, adv_loss_w=0.1, l1_loss_w=1, style_loss_w=250, perc_loss_w=0.1, train=False):
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.edge_model = EdgeModel(n_blocks, edge_lr, fm_loss_w)
        self.inpaint_model = InpaintingModel(n_blocks, inpaint_lr, adv_loss_w, l1_loss_w, style_loss_w, perc_loss_w)
        if train:
            data_root = os.path.join(os.path.pardir, 'data')
            self.all_data = TrainingDataset(os.path.join(data_root, 'celeba64'), os.path.join(data_root, 'qd_imd64', 'train'))
            train_sampler, test_sampler = self.load_data()
            self.train_loader = DataLoader(self.all_data, batch_size, sampler=train_sampler, drop_last=True)
            self.test_loader = DataLoader(self.all_data, batch_size, sampler=test_sampler, drop_last=True)
        self.batch_size = batch_size

    def load_data(self):
        omit_percentage = 0.9  # For debug and runtime bounding
        test_percentage = 0.2
        all_indices = range(len(self.all_data))
        split_index = int(len(self.all_data) * (1 - omit_percentage))
        used_indices = all_indices[:split_index]
        train_test_index = int(len(used_indices) * (1 - test_percentage))
        train_indices = used_indices[:train_test_index]
        test_indices = used_indices[train_test_index:]
        train_sampler = SubsetRandomSampler(train_indices)
        test_sampler = SubsetRandomSampler(test_indices)
        return train_sampler, test_sampler

    def to_cuda(self, items):
        return (item.to(self.device, dtype=torch.float) for item in items)

    def train(self, n_epochs):
        self.edge_model.train()
        self.inpaint_model.train()
        self.history = {
            'edge_gen': [],
            'edge_dis': [],
            'paint_gen': [],
            'paint_dis': []
        }
        for e in range(n_epochs):
            print('Epoch:', e + 1)
            for i, data in enumerate(self.train_loader):
                if i % 10 == 0:
                    print(i/len(self.train_loader) * 100)
                images, gray_images, edges, masks = self.to_cuda(data)
                images = self.reshape_image(images)
                gray_images = self.reshape_image(gray_images)
                edges = self.reshape_image(edges)
                masks = self.reshape_image(masks)

                edge_out, edge_gen_loss, edge_dis_loss = self.edge_model.epoch(gray_images, edges, masks)
                masked_edge_output = edge_out * masks + edges * (1 - masks)
                inpaint_out, inpaint_gen_loss, inpaint_dis_loss = self.inpaint_model.epoch(images, masked_edge_output, masks)
                merged_output = inpaint_out * masks + images * (1 - masks)
                self.history['edge_gen'].append(edge_gen_loss.item())
                self.history['edge_dis'].append(edge_dis_loss.item())
                self.history['paint_gen'].append(inpaint_gen_loss.item())
                self.history['paint_dis'].append(inpaint_dis_loss.item())

                self.edge_model.backward(edge_gen_loss, edge_dis_loss)
                self.inpaint_model.backward(inpaint_gen_loss, inpaint_dis_loss)

    def predict(self, image, mask):
        mask = rgb2gray(mask)
        mask[mask < 0.95] = 0
        mask = np.round(mask)
        gray_image = rgb2gray(image)
        masked_gray_image = gray_image * mask  # The masks are not as expected, but I fixed it in the Dataset class
        masked_edges = get_edges(gray_image, mask)
        masked_gray_tensor = self.get_tensor_sample(masked_gray_image)
        masked_edge_tensor = self.get_tensor_sample(masked_edges)
        mask_tensor = self.get_tensor_sample(mask)
        edge_model_input = torch.cat((masked_gray_tensor, masked_edge_tensor, mask_tensor), dim=1)
        edge_map = self.edge_model.generator(edge_model_input)

        masked_image = image * np.reshape(mask, (64, 64, 1))
        masked_image_tensor = self.get_tensor_sample(masked_image)
        inpaint_input = torch.cat((masked_image_tensor, edge_map), dim=1)
        output = self.inpaint_model.generator(inpaint_input)
        #merged_output = output
        #merged_output = 255 * output * (1 - mask_tensor) + masked_image_tensor
        return (output, mask_tensor, masked_image_tensor), edge_map


    def show_tensor(self, tensor, show=False):
        numpy_representation = tensor.cpu().data.numpy().reshape((64, 64, -1))
        if numpy_representation.shape[2] == 1:
            numpy_representation = numpy_representation.reshape((64, 64))
            #io.imshow(numpy_representation, cmap='gray')
            #plt.title('Edge model output')
            if show:
                io.show()
        else:
            #io.imshow(numpy_representation)
            #cv2.imshow('image', numpy_representation)
            #cv2.waitKey(0)
            if show:
                io.show()
        return numpy_representation

    def get_tensor_sample(self, image):
        n_channels = 1 if len(image.shape) == 2 else image.shape[2]
        cuda_tensor = torch.tensor(image).to(self.device, dtype=torch.float)
        reshaped_tensor = torch.reshape(cuda_tensor, (1, n_channels, 64, 64))
        return reshaped_tensor



    def reshape_image(self, image):
        n_channels = 1 if len(image.shape) == 3 else image.shape[3]
        image = torch.reshape(image, (self.batch_size, n_channels, image.shape[1], image.shape[2]))
        return image

    def save(self, path, name):
        edge_path = os.path.join(path, 'edge_model_{}.pth'.format(name))
        inpaint_path = os.path.join(path, 'inpaint_model_{}.pth'.format(name))
        torch.save(self.edge_model.state_dict(), edge_path)
        torch.save(self.inpaint_model.state_dict(), inpaint_path)
        np.save('history_{}'.format(name), self.history)

    def load(self, path, name):
        edge_path = os.path.join(path, 'edge_model_{}.pth'.format(name))
        inpaint_path = os.path.join(path, 'inpaint_model.pth'.format(name))
        self.edge_model.load_state_dict(torch.load(edge_path))
        self.inpaint_model.load_state_dict(torch.load(inpaint_path))

    def construct_inpaint_image(self, generator_output, mask_tensor, masked_image):
        output = self.show_tensor(generator_output)
        mask = np.reshape(self.show_tensor(mask_tensor), (64, 64, 1))
        masked_image = self.show_tensor(masked_image) * 255

        inpainted_image = 255 * output * (1 - mask) + masked_image
        return inpainted_image

def load_test_sample():
    img_path = 'data\\celeba64\\000001.jpg'
    mask_path = 'data\\qd_imd64\\train\\00000_train.png'
    image = io.imread(img_path)
    mask = io.imread(mask_path)
    return image, mask

def get_edges(image, mask):
    mask = mask.astype(np.bool)
    sigma = 1.
    masked_edges = canny(image, sigma=sigma, mask=mask).astype(np.float)
    return masked_edges


def create_sample_outputs(model_path, model_name, samples_train_test):
    ec = EdgeConnect(train=True)
    ec.load(model_path, model_name)
    n_samples = len(ec.all_data)
    train_indices = range(1, int(0.05 * n_samples))  # Generally not true. Probably okay tho
    test_indices = range(n_samples, int(0.95 * n_samples), -1)
    mask_indices = range(30)
    for i in range(samples_train_test):
        train_img_index = np.random.choice(train_indices)
        test_img_index = np.random.choice(test_indices)
        mask_index = np.random.choice(mask_indices)

        train_file_name = str(train_img_index).zfill(6)
        test_file_name = str(test_img_index).zfill(6)
        mask_file_name = str(mask_index).zfill(5) + '_train.png'

        train_img = io.imread('..\\data\\celeba64\\{}.jpg'.format(train_file_name)) / 255
        test_img = io.imread('..\\data\\celeba64\\{}.jpg'.format(test_file_name)) / 255
        mask = io.imread('..\\data\\qd_imd64\\train\\{}'.format(mask_file_name))

        train_output_components, train_edge_map = ec.predict(train_img, mask)
        test_output_components, test_edge_map = ec.predict(test_img, mask)

        train_output_img = ec.construct_inpaint_image(*train_output_components)
        train_edge_img = ec.show_tensor(train_edge_map)
        test_output_img = ec.construct_inpaint_image(*test_output_components)
        test_edge_img = ec.show_tensor(test_edge_map)

        train_results_path = '..\\outputs\\train'
        test_results_path = '..\\outputs\\test'

        io.imsave(train_results_path + '\\{}.jpg'.format(train_file_name), train_img)
        io.imsave(train_results_path + '\\{}_edges.jpg'.format(train_file_name), train_edge_img)
        io.imsave(train_results_path + '\\{}_output.jpg'.format(train_file_name), train_output_img)

        io.imsave(test_results_path + '\\{}.jpg'.format(test_file_name), test_img)
        io.imsave(test_results_path + '\\{}_edges.jpg'.format(test_file_name), test_edge_img)
        io.imsave(test_results_path + '\\{}_output.jpg'.format(test_file_name), test_output_img)

def plot_losses(path, epochs=None):
    history = np.load(path, allow_pickle=True).item()
    edge_gen_loss = history['edge_gen'] if epochs is None else [np.mean(x) for x in np.split(np.array(history['edge_gen']), epochs)]
    edge_dis_loss = history['edge_dis'] if epochs is None else [np.mean(x) for x in np.split(np.array(history['edge_dis']), epochs)]
    inpaint_gen_loss = history['paint_gen'] if epochs is None else [np.mean(x) for x in np.split(np.array(history['paint_gen']), epochs)]
    inpaint_dis_loss = history['paint_dis'] if epochs is None else [np.mean(x) for x in np.split(np.array(history['paint_dis']), epochs)]


    fig, axis = plt.subplots(4, 1, sharex=True)
    #fig.subplots_adjust(hspace=0)
    axis[0].plot(edge_gen_loss, c='limegreen', label='Edge model generator')
    axis[1].plot(edge_dis_loss, c='forestgreen', label='Edge model discriminator')
    axis[2].plot(inpaint_gen_loss, c='royalblue', label='Inpainting model generator')
    axis[3].plot(inpaint_dis_loss, c='navy', label='Inpainting model discriminator')
    axis[0].legend()
    axis[1].legend()
    axis[2].legend()
    axis[3].legend()
    #plt.legend()
    plt.show()


def get_defaults():
    params = {
        'l1_w': 1,
        'fm_w': 10,
        'style_w': 250,
        'perc_w': 0.1,
        'inpaint_w': 0.1,
        'edge_lr': 1e-4,
        'inpaint_lr': 1e-4
    }

def run(name, epochs):
    ec = EdgeConnect(train=True)
    ec.train(epochs)
    ec.save(os.path.join('models', 'model'), name)

#plot_losses('history_beta_03_noise.npy', 20)
#run('beta_02_noise', 15)
#history = np.load('history_05.npy', allow_pickle=True).item()

#means = [np.mean(x) for x in np.split(np.array(history['paint_dis']), 15)]
#plt.plot(means)
#plt.show()
#print()
#create_sample_outputs('models\\model', 'beta_02_noise', 20)