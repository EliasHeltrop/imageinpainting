import torch
import torch.nn as nn
from torchvision import models

class AdversarialLoss(nn.Module):
    def __init__(self):
        super(AdversarialLoss, self).__init__()
        self.criterion = nn.BCELoss()
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    def __call__(self, outputs, real_image):
        labels = torch.tensor(1.) if real_image else torch.tensor(0.)
        labels = labels.expand_as(outputs).to(self.device)
        loss = self.criterion(outputs, labels)
        return loss

class StyleLoss(nn.Module):
    def __init__(self):
        super(StyleLoss, self).__init__()
        self.vgg = VGG19()
        self.criterion = nn.L1Loss()

    def gram_matrix(self, x):
        bs, ch, h, w = x.size()
        phi = x.view(bs, ch, h * w)
        phi_T = torch.transpose(phi, 1, 2)
        gram_matrix = torch.bmm(phi, phi_T) / (h * w * ch)
        return gram_matrix

    def __call__(self, x, y):
        out_x = self.vgg(x)
        out_y = self.vgg(y)

        style_loss = 0.0
        style_loss += self.criterion(self.gram_matrix(out_x['relu2_2']), self.gram_matrix(out_y['relu2_2']))
        style_loss += self.criterion(self.gram_matrix(out_x['relu3_4']), self.gram_matrix(out_y['relu3_4']))
        style_loss += self.criterion(self.gram_matrix(out_x['relu4_4']), self.gram_matrix(out_y['relu4_4']))
        style_loss += self.criterion(self.gram_matrix(out_x['relu5_2']), self.gram_matrix(out_y['relu5_2']))
        return style_loss

class PerceptualLoss(nn.Module):
    def __init__(self):
        super(PerceptualLoss, self).__init__()
        self.vgg = VGG19()
        self.criterion = nn.L1Loss()

    def __call__(self, x, y):
        out_x = self.vgg(x)
        out_y = self.vgg(y)

        perc_loss = 0.0
        perc_loss += self.criterion(out_x['relu1_1'], out_y['relu1_1'])
        perc_loss += self.criterion(out_x['relu2_1'], out_y['relu2_1'])
        perc_loss += self.criterion(out_x['relu3_1'], out_y['relu3_1'])
        perc_loss += self.criterion(out_x['relu4_1'], out_y['relu4_1'])
        perc_loss += self.criterion(out_x['relu5_1'], out_y['relu5_1'])
        return perc_loss


class VGG19(torch.nn.Module):
    """
    I don't know what the fuck that means. What an abomination.
    """
    def __init__(self):
        super(VGG19, self).__init__()
        features = models.vgg19(pretrained=True).features
        self.relu1_1 = torch.nn.Sequential()
        self.relu1_2 = torch.nn.Sequential()

        self.relu2_1 = torch.nn.Sequential()
        self.relu2_2 = torch.nn.Sequential()

        self.relu3_1 = torch.nn.Sequential()
        self.relu3_2 = torch.nn.Sequential()
        self.relu3_3 = torch.nn.Sequential()
        self.relu3_4 = torch.nn.Sequential()

        self.relu4_1 = torch.nn.Sequential()
        self.relu4_2 = torch.nn.Sequential()
        self.relu4_3 = torch.nn.Sequential()
        self.relu4_4 = torch.nn.Sequential()

        self.relu5_1 = torch.nn.Sequential()
        self.relu5_2 = torch.nn.Sequential()
        self.relu5_3 = torch.nn.Sequential()
        self.relu5_4 = torch.nn.Sequential()

        for x in range(2):
            self.relu1_1.add_module(str(x), features[x])

        for x in range(2, 4):
            self.relu1_2.add_module(str(x), features[x])

        for x in range(4, 7):
            self.relu2_1.add_module(str(x), features[x])

        for x in range(7, 9):
            self.relu2_2.add_module(str(x), features[x])

        for x in range(9, 12):
            self.relu3_1.add_module(str(x), features[x])

        for x in range(12, 14):
            self.relu3_2.add_module(str(x), features[x])

        for x in range(14, 16):
            self.relu3_3.add_module(str(x), features[x])

        for x in range(16, 18):
            self.relu3_4.add_module(str(x), features[x])

        for x in range(18, 21):
            self.relu4_1.add_module(str(x), features[x])

        for x in range(21, 23):
            self.relu4_2.add_module(str(x), features[x])

        for x in range(23, 25):
            self.relu4_3.add_module(str(x), features[x])

        for x in range(25, 27):
            self.relu4_4.add_module(str(x), features[x])

        for x in range(27, 30):
            self.relu5_1.add_module(str(x), features[x])

        for x in range(30, 32):
            self.relu5_2.add_module(str(x), features[x])

        for x in range(32, 34):
            self.relu5_3.add_module(str(x), features[x])

        for x in range(34, 36):
            self.relu5_4.add_module(str(x), features[x])

        # don't need the gradients, just want the features
        for param in self.parameters():
            param.requires_grad = False

    def forward(self, x):
        relu1_1 = self.relu1_1(x)
        relu1_2 = self.relu1_2(relu1_1)

        relu2_1 = self.relu2_1(relu1_2)
        relu2_2 = self.relu2_2(relu2_1)

        relu3_1 = self.relu3_1(relu2_2)
        relu3_2 = self.relu3_2(relu3_1)
        relu3_3 = self.relu3_3(relu3_2)
        relu3_4 = self.relu3_4(relu3_3)

        relu4_1 = self.relu4_1(relu3_4)
        relu4_2 = self.relu4_2(relu4_1)
        relu4_3 = self.relu4_3(relu4_2)
        relu4_4 = self.relu4_4(relu4_3)

        relu5_1 = self.relu5_1(relu4_4)
        relu5_2 = self.relu5_2(relu5_1)
        relu5_3 = self.relu5_3(relu5_2)
        relu5_4 = self.relu5_4(relu5_3)

        out = {
            'relu1_1': relu1_1,
            'relu1_2': relu1_2,

            'relu2_1': relu2_1,
            'relu2_2': relu2_2,

            'relu3_1': relu3_1,
            'relu3_2': relu3_2,
            'relu3_3': relu3_3,
            'relu3_4': relu3_4,

            'relu4_1': relu4_1,
            'relu4_2': relu4_2,
            'relu4_3': relu4_3,
            'relu4_4': relu4_4,

            'relu5_1': relu5_1,
            'relu5_2': relu5_2,
            'relu5_3': relu5_3,
            'relu5_4': relu5_4,
        }
        return out