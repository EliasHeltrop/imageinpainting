import tkinter.filedialog
import tkinter as tk
from PIL import ImageTk, Image
import os
import numpy as np
from skimage import io
from skimage.transform import resize
from architecture.main import EdgeConnect, load_test_sample

class GUI:
    def __init__(self):
        self.app = tk.Tk()
        self.app.title('Image Inpainting')
        self.image_size = (178, 218)
        #self.bg_file = os.path.join('image_samples', '000001.jpg')
        self.bg_file = 'image_samples/000001.jpg'
        self.image = ImageTk.PhotoImage(Image.open(self.bg_file))
        self.edge_image = ImageTk.PhotoImage(Image.open('edges_blank.jpg'))
        self.inpaint = ImageTk.PhotoImage(Image.open('inpaint_blank.jpg'))

        self.positions = []
        self.current_x, self.current_y = 0., 0.

        self.mask = None
        self.src_image = None

        frame = tk.Frame(self.app)

        self.canvas = tk.Canvas(self.app, width= self.image_size[0], height=self.image_size[1], cursor='cross')
        self.canvas_2 = tk.Canvas(self.app, width=250, height=100)

        self.canvas_2.pack()
        self.canvas_2.create_image(65, 50, image=self.edge_image)
        self.canvas_2.create_image(185, 50, image=self.inpaint)
        self.canvas.pack(expand=False)
        self.set_image = self.canvas.create_image(90, 100, image=self.image, anchor='center')

        self.button_select = tk.Button(self.app, text='Select image', command=self.select_image)
        self.button_select.pack(side="top", fill="both", expand=True)
        self.button_clear = tk.Button(self.app, text="Clear", command=self.clear_all)
        self.button_clear.pack(side="top", fill="both", expand=True)
        self.button_print = tk.Button(self.app, text="Inpaint", command=self.print_positions)
        self.button_print.pack(side="top", fill="both", expand=True)

        self.canvas.bind("<Motion>", self.get_position)
        self.canvas.bind("<B1-Motion>", self.draw)
        self.app.mainloop()


    def select_image(self):
        file = tk.filedialog.askopenfilename(initialdir='image_samples',
                                             title='Select image')
        self.bg_file = file
        self.image = ImageTk.PhotoImage(Image.open(self.bg_file))
        self.canvas.create_image(90, 100, image=self.image, anchor='center')

    def clear_all(self):
        self.canvas.delete("all")

    def draw(self, event):
        self.x = event.x
        self.y = event.y
        self.canvas.create_line(self.current_x, self.current_y, self.x, self.y, fill="black")
        #self.positions.append((self.current_x, self.current_y))
        self.positions.append((self.x, self.y))
        self.current_x = self.x
        self.current_y = self.y

    def get_position(self, event):
        self.current_x = event.x
        self.current_y = event.y

    def print_positions(self):
        mask = np.ones((self.image_size[0], self.image_size[1]))
        for point in self.positions:
            x = point[0]
            y = point[1]
            if x < self.image_size[0] and y < self.image_size[1]:
                mask[x, y] = 0.
        self.positions = []
        down_sampled_mask = shrink_image(mask).T
        image_path = self.bg_file.split('/')[-1]
        down_sampled_image = io.imread(os.path.join('data', 'celeba64', image_path))

        #down_sampled_image, down_sampled_mask = load_test_sample()
        model = EdgeConnect()
        model.load(os.path.join('architecture', 'models', 'model'), 'beta_02_noise')
        inpaint_outputs, edges = model.predict(down_sampled_image / 255, down_sampled_mask)
        output = model.construct_inpaint_image(*inpaint_outputs)
        edge_image = resize(model.show_tensor(edges, False), (100, 100))
        inpainted_image = resize(output.astype('uint8'), (100, 100))
        io.imsave('edges.jpg', edge_image)
        io.imsave('inpainted_image.jpg', inpainted_image)
        offset_x = 15
        self.edge_image = ImageTk.PhotoImage(Image.open('edges.jpg'))
        self.inpaint = ImageTk.PhotoImage(Image.open('inpainted_image.jpg'))
        self.canvas_2.create_image(50+offset_x, 50, image=self.edge_image)
        self.canvas_2.create_image(170+offset_x, 50, image =self.inpaint)

def shrink_image(image):
    downsampled = resize(image, (64, 64))
    return downsampled

if __name__ == "__main__":
    GUI()