# ImageInpainting

Dataset used: CelebA aligned 
masks dataset: https://github.com/karfly/qd-imd

"""
The following data_structure is assumed:
----------------------------------------
For the downsampled CelebA dataset:

data/celeba64/all-the-jpg-files
----------------------------------------
For the mask dataset:
data/qd_imd64/  -train/all-train.png
                -test/all-test.png
----------------------------------------
"""

